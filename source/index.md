---
title: Locent Public API

language_tabs:
  - json

toc_footers:
  - <a href='http://github.com/tripit/slate'>Documentation Powered by Slate</a>

search: true
---

# Locent Public API

<aside class="notice">
The Locent API V1 exists at locent-api.herokuapp.com/api
</aside>

All requests need to be authorized using the authorization token (from your dashboard). This token needs to be passed in your request’s header using the Authorization field.

`Authorization: Token token=YourOwnUniqueToken`

# Opt Ins

The flow for opting in a customer or a group of customers to a feature is as follows:

**1.** When a customer has indicated (on your website or application) that he/she would like to subscribe to/opt in for either `Keyword (id = 1), Clearcart (id = 2)` or `Safetext (id = 3)`, a POST request to /opt_ins/ should be made containing the customer(s) details and feature id.

> Below is an example of the body/parameters to be sent when making a bulk opt-in request:

```json
{
    "feature_id" : "3",
    "customers" : [
            {
                "uid" : "dwbefkwefwhjkd3221e3eekwfw",
                "first_name" : "Chris",
                "last_name" : "Ishida",
                "phone_number" : "+18*********"
            },
            {
                "uid" : "fefyyefeuf33u9i39ir4r4f",
                "first_name" : "Matt",
                "last_name" : "Joseph",
                "phone_number" : "+17*********"
            }
        ]
}
```

*`"feature_id"`, `"uid"` and `"phone_number"` are required fields.*

**2.** Locent would then make a POST request to the Opt In Verification URL (for SafeText) provided by you/your organization via the dashboard. Your organization may have different Opt In Verification URLs for each feature. The request body would contain each customer’s Two-Factor Verification code.

> Below is an example of the body of the request:

```json
[
    {
        "customer_uid" : "dwbefkwefwhjkd3221e3eekwfw",
        "verification_code" : "34HHFN"
    },
    {
        "customer_uid" : "fefyyefeuf33u9i39ir4r4f",
        "verification_code" : "28JEH2"
    },
]
```

**3.** The Two-Factor Verification Code(s) should be displayed to the customer(s) (e.g somewhere on the mobile application/website). A text message would automatically be sent by Locent, to the customer asking them to verify their opt in by replying their verification code or to cancel it by texting back `NO`.


# Safetext

The flow for initiating and handling Safetext instances is as follows:

**1.** When a customer intiates a purchase on your e-commerce website or application, to set off Safetext, a POST request to /safetext should be made with the following parameters:

Parameter | Description | Required
----------|-------------|----------
`customer_uid` |	(Globally unique) universal ID of the customer.	| true
`customer_phone_number` | The full phone number of the customer.	| true
`order_uid`	| Unique identifier of the order that’s being placed by the customer. | true
`item_name` | Name of item being purchased. | true
`item_price` | The price of the item. Without the currency sign. | true


**2.** Locent would SMS your customer, asking if they want to proceed with the order. The customer is to reply with either PAY or NO to confirm/cancel the order respectively. Once this reply is recieved, Locent would send a POST request to the Safetext `Purchase Request URL` you provided via your dashboard.

> An example of the request is below:

```json
{
    "status": "confirmed",
    "order_uid" : "unique-order-id"
}
```

*Note that `"status"` can also have a value of `"cancelled"`.*

**3.** Upon getting above information from Locent about the customer’s intention, your application should now proceed to either process the order or cancel it the way it usually would. Locent would need to be notified once this is done, so as to send the user a corresponding “Your order is on it’s way…” or “Your order has successfully been cancelled…” text message. To do this, send a POST request to `/order_status` with the following parameters.

Parameter | Description | Required
----------|-------------|----------
`order_uid` | Unique identifier of the order that’s being placed by the customer | true
`order_success` | true or false | true
`error_code` |1001, 1002 or 1003 | would be ignored if order_success isn't false


Locent can send an error message to your customer depending on what kind of error it is. You can customize these messages in your Dashboard settings.

Error codes and description:

- 1001: Server Error
- 1002: Product out of stock
- 1003: Insufficient funds


# Keyword

You can add products that your customers can purchase with Keyword via your dashboard. When a customer tries to buy an item by texting the keyword of that item to your Locent phone number, Locent would send a POST request to the Safetext `Purchase Request URL` you provided via your dashboard. 

> An example of the request is below:

```json
{
    "status": "confirmed", 
    "order_uid" : "unique-order-id"
}
```

*Note that `"status"` can also have a value of `"cancelled"`.*

Upon getting above information from Locent about the customer’s intention, your application should now proceed to either process the order or cancel it the way it usually would. Locent would need to be notified once this is done, so as to send the user a corresponding “Your order is on it’s way…” or “Your order has successfully been cancelled…” text message. To do this, send a POST request to `/order_status` with the following parameters.

Parameter | Description | Required
----------|-------------|-----------
`order_uid` | Unique identifier of the order that’s being placed by the customer | true
`order_success` | true or false | true
`error_code` |1001, 1002 or 1003 | would be ignored if order_success isn't false


Locent can send an error message to your customer depending on what kind of error it is. You can customize these messages in your Dashboard settings.

Error codes and description:

- 1001: Server Error
- 1002: Product out of stock
- 1003: Insufficient funds